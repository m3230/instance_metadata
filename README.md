# **Instance_metadata**

Querying the metadata of an instance within AWS and provide a json formatted output.

**Cloud Provider - AWS** 

**Programming Language - Python**

**Prerequisites**
- EC2 Linux instance on AWS
- SSH to EC2 instance
- Install Python3

**Command to execute**
- git clone git@gitlab.com:m3230/instance_metadata.git
- cd instance_metadata/src
- python3 metadata.py or querymetadata.py


